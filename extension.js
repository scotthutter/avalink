// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
var WebSocketServer = require('websocket').server;
var http = require('http');

var avalinkconfig = "";

//import {window, commands, Disposable, ExtensionContext, StatusBarAlignment, StatusBarItem, TextDocument} from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('AVA Link is now active');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('extension.gotoDocument', function () {
        // The code you place here will be executed every time your command is executed

        

        // Get local js file path
        try {
            var fs = require('fs');
            var myExtDir = vscode.extensions.getExtension("Asurion.avalink").extensionPath;
            avalinkconfig = JSON.parse(fs.readFileSync(myExtDir + '/avalinkconfig.json'), 'utf8');
        }
        catch(err)
        {
            vscode.window.showInformationMessage('Error: ' + err);
        }
        


        // Display a message box to the user
        vscode.window.showInformationMessage('AVA Link Active');

        var server = http.createServer(function(request, response) {
            // process HTTP request. Since we're writing just WebSockets
            // server we don't have to implement anything.
          });
          server.listen(8180, function() { });
          
          // create the server
          wsServer = new WebSocketServer({
            httpServer: server
          });
          var clients = [];

          // WebSocket server
          wsServer.on('request', function(request) {
            var connection = request.accept(null, request.origin);
            clients.push(connection);
          
            // This is the most important callback for us, we'll handle
            // all messages from users here.
            connection.on('message', function(message) {
              if (message.type === 'utf8') {
                // process WebSocket message
                var parms = message.utf8Data.split(" ");

                if(parms[0].toUpperCase() == "OPEN")
                {
                    var filePath = "file:///" + avalinkconfig.jsfilepath + parms[1];

                    var openPath = vscode.Uri.parse(filePath); //A request file path
                    vscode.workspace.openTextDocument(openPath).then(doc => {
                        vscode.window.showTextDocument(doc);                        

                    }).catch(err => console.log(err)) // operation failed;
                }

                if(parms[0].toUpperCase() == "LINE")
                {
                    var linenum = parseInt(parms[1]);
                    let editor = vscode.window.activeTextEditor;
                    let range = editor.document.lineAt(linenum-1).range;
                    editor.selection =  new vscode.Selection(range.start, range.end);
                    editor.revealRange(range);
                }

                if(parms[0].toUpperCase() == "FIND")
                {
                    let editor = vscode.window.activeTextEditor;
                    let doc = editor.document;
                    let text = doc.getText();
                    let match = "";
                    let regEx = new RegExp(parms[1], "g");

                    while (match = regEx.exec(text)) {

                        let startPos = doc.positionAt(match.index);
                        let endPos = doc.positionAt(match.index + match[0].length);
                        let range = new vscode.Range(startPos, endPos);
                        editor.selection =  new vscode.Selection(range.start, range.end);
                        editor.revealRange(range);
                    }
                }

                if(parms[0].toUpperCase() == "FIND1")
                {
                    let editor = vscode.window.activeTextEditor;
                    let doc = editor.document;
                    let text = doc.getText();
                    let match = "";
                    let regEx = new RegExp(parms[1], "g");

                    while (match = regEx.exec(text)) {

                        let startPos = doc.positionAt(match.index);
                        let endPos = doc.positionAt(match.index + match[0].length);
                        let range = new vscode.Range(startPos, endPos);
                        editor.selection =  new vscode.Selection(range.start, range.end);
                        editor.revealRange(range);
                        //vscode.Breakpoint.

                        clients.forEach(function(client) {
                            client.send("SELECT " + parms[1]);
                          });
                        //connection.send("SELECT " + parms[1]);

                        break;
                    } 
                }


              }
            });
          
            connection.on('close', function(connection) {
              // close user connection
            });
          });
       
    });

    context.subscriptions.push(disposable);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {
}
exports.deactivate = deactivate;